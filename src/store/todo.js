export default {
  state: () => ({
    todo: [
      { id: 0, title: "Kill Elon", done: true },
      { id: 1, title: "Kill amber heard", done: false },
      { id: 2, title: "resurrect Elon Musk", done: false },
      { id: 3, title: "Kill youself", done: false },
      { id: 4, title: "resurrect yourself", done: true },
    ],
  }),
  mutations: {
    done(state, id) {
      state.todo.map((item) => (item.id === id ? (item.done = true) : item));
    },
    unDone(state, id) {
      state.todo.map((item) => (item.id === id ? (item.done = false) : item));
    },
    addTodo(state, todo) {
      const len = state.todo.length;
      state.todo.push({ id: len, title: todo, done: false });
    },
    deleteTodo(state, id) {
      state.todo = state.todo.filter((item) => item.id !== id);
    },
  },
  actions: {
    makeDone({ commit }, id) {
      commit("done", id);
    },
    makeUnDone({ commit }, id) {
      commit("unDone", id);
    },
    addTodo({ commit }, title) {
      commit("addTodo", title);
    },
    deleteTodo({ commit }, id) {
      commit("deleteTodo", id);
    },
  },
  getters: {
    getDone(state) {
      return state.todo.filter((todo) => todo.done);
    },
    getUndone(state) {
      return state.todo.filter((todo) => !todo.done);
    },
  },
};
